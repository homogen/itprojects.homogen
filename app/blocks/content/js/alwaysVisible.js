/* script */

(function (window, $) {
    'use strict';
    var scrolled = function () {
        var offset = $("header").outerHeight() + $(".description").outerHeight() + 40;
        var scrollTop = Math.max(offset, window.scrollY);
        if (scrollTop > offset) {
            $(".alwaysVisible").addClass("sticky").css({top: scrollTop + $("header").outerHeight()});
        }
        else {
            $(".alwaysVisible").removeClass("sticky").css({top: offset});
        }
    };
    scrolled();
    $(window).on("orientationchange scroll", scrolled);
})(window, jQuery);
